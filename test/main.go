package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/patrickmn/go-cache"
)

var OkResult = ResultStatus{Status: "ok"}

type ResultStatus struct {
	Status string `json:"status"`
}

type ResultPagination struct {
	Elements interface{} `json:"elements"`
	Total    int         `json:"total"`
}

type Error struct {
	Error string `json:"error"`
}

func main() {
	route := newRouter()

	srv := newServer(route, ":85")

	go func() {
		log.Print("server start")
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	srv.Shutdown(ctx)
}

func newServer(r http.Handler, addr string) *http.Server {
	return &http.Server{
		Handler: r,
		Addr:    addr,
	}
}

func newRouter() http.Handler {
	r := chi.NewRouter()

	// product routes //
	ch := cache.New(1*time.Hour, 1*time.Hour)
	pSt := productStorage{ch: ch, m: []int{}}

	r.Post("/product", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		product := Product{}
		ok := populateFromRequestData(w, r, &product)
		if !ok {
			return
		}

		pSt.SetProduct(&product)

		sendResult(w, product)
	})

	r.Get("/product/{id}", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		var (
			id  int
			err error
		)

		if chi.URLParam(r, "id") == "" {
			wrapError(w, "request parameter 'id' is required", http.StatusBadRequest)
			return
		} else {
			id, err = strconv.Atoi(chi.URLParam(r, "id"))
			if err != nil {
				wrapError(w, err.Error(), http.StatusBadRequest)
			}
		}

		prod, ok := pSt.GetProduct(id)
		if !ok {
			wrapError(w, fmt.Sprintf("not found entry by id %d", id), http.StatusNotFound)
			return
		}

		sendResult(w, prod)
	})

	r.Get("/product", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		offset, limit, err := getPaginationParams(r)
		if err != nil {
			wrapError(w, err.Error(), http.StatusBadRequest)
			return
		}

		prods, err := pSt.GetProducts(offset, limit)
		if err != nil {
			wrapError(w, err.Error(), http.StatusBadRequest)
			return
		}

		sendResult(w, ResultPagination{Elements: prods, Total: len(pSt.m)})
	})

	r.Delete("/product/{id}", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		id := chi.URLParam(r, "id")
		if id == "" {
			wrapError(w, "request parameter 'id' is required", http.StatusBadRequest)
			return
		}

		pSt.DeleteProduct(id)

		sendResult(w, OkResult)
	})

	// order routes //
	cho := cache.New(1*time.Hour, 1*time.Hour)
	oSt := ordersStorage{ch: cho, m: []string{}}
	r.Post("/order", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		orderCreate := OrderCreate{}
		ok := populateFromRequestData(w, r, &orderCreate)
		if !ok {
			return
		}

		// validate
		for i := range orderCreate.Products {
			_, ok := pSt.GetProduct(orderCreate.Products[i].Id)
			if !ok {
				wrapError(w, fmt.Sprintf("not found entry by id %d", orderCreate.Products[i].Id), http.StatusNotFound)
				return
			}
		}

		o := oSt.SetOrder(orderCreate)

		sendResult(w, o)
	})

	r.Get("/order/{id}", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		id := chi.URLParam(r, "id")
		if id == "" {
			wrapError(w, "request parameter 'id' is required", http.StatusBadRequest)
			return
		}

		order, ok := oSt.GetOrder(id)
		if !ok {
			wrapError(w, "not found entry by id "+id, http.StatusNotFound)
			return
		}

		sendResult(w, order)
	})

	r.Get("/order", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		offset, limit, err := getPaginationParams(r)
		if err != nil {
			wrapError(w, err.Error(), http.StatusBadRequest)
			return
		}

		orders, e := oSt.GetOrders(offset, limit)
		if e != nil {
			wrapError(w, e.Error(), http.StatusBadRequest)
			return
		}

		sendResult(w, ResultPagination{Elements: orders, Total: len(pSt.m)})
	})

	return r
}

func sendResult(w http.ResponseWriter, data interface{}) {
	resByte, err := json.Marshal(data)
	if err != nil {
		log.Print("can't marshal resStruct ", err)
		wrapError(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(resByte)
}

func getPaginationParams(r *http.Request) (int, int, error) {
	var err error
	query := r.URL.Query()

	offset := 0
	offsetParam, ok := query["offset"]
	if ok && len(offsetParam) > 0 {
		offset, err = strconv.Atoi(offsetParam[0])
		if err != nil {
			return 0, 0, err
		}
	}

	limit := 0
	limitParam, ok := query["limit"]
	if ok && len(limitParam) > 0 {
		limit, err = strconv.Atoi(limitParam[0])
		if err != nil {
			return 0, 0, err
		}
	}

	return offset, limit, nil
}

func wrapError(w http.ResponseWriter, err string, code int) {
	resByte, errMarshal := json.Marshal(Error{
		Error: err,
	})
	if errMarshal != nil {
		w.WriteHeader(code)
		return
	}

	w.WriteHeader(code)
	w.Write(resByte)
}

func populateFromRequestData(w http.ResponseWriter, r *http.Request, data interface{}) bool {
	body, _ := ioutil.ReadAll(r.Body)

	err := json.Unmarshal(body, &data)
	if err != nil {
		log.Print("can't unmarshal Request ", err)
		wrapError(w, err.Error(), http.StatusBadRequest)
		return false
	}

	return true
}

///////////////
// Product
///////////////

type CreateProductRequest struct {
	Name  string `json:"name"`
	Price int    `json:"price"`
}

type Product struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Price int    `json:"price"`
}

type ProductStorage interface {
	SetProduct(*Product)
	GetProduct(name string) (*Product, bool)
	GetProducts(offset int, limit int) ([]Product, error)
	DeleteProduct(name string)
}

type productStorage struct {
	ch *cache.Cache
	m  []int
}

func (s *productStorage) SetProduct(p *Product) {
	if len(s.m) > 0 {
		key := len(s.m) - 1
		p.Id = s.m[key] + 1
	} else {
		p.Id = 1
	}
	s.ch.Set(fmt.Sprintf("%d", p.Id), p, cache.DefaultExpiration)
	s.m = append(s.m, p.Id)
}

func (s *productStorage) GetProduct(id int) (*Product, bool) {
	t, ok := s.ch.Get(fmt.Sprintf("%d", id))
	if !ok {
		return nil, false
	}

	return t.(*Product), true
}

func (s *productStorage) GetProducts(offset int, limit int) ([]Product, error) {
	if offset > len(s.m) {
		return []Product{}, errors.New("you looking products out of range")
	}

	cap := limit
	if limit == 0 {
		cap = len(s.m) - offset
	}
	result := make([]Product, 0, cap)

	for i := offset; i < len(s.m); i++ {
		if limit != 0 && i >= offset+limit {
			break
		}

		prod, ok := s.GetProduct(s.m[i])
		if !ok {
			fmt.Println(i)
			return []Product{}, errors.New(fmt.Sprintf("not found entry by id %d", s.m[i]))
		}
		result = append(result, *prod)
	}

	return result, nil
}

func (s *productStorage) DeleteProduct(id string) {
	s.ch.Delete(string(id))

	for i := range s.m {
		if fmt.Sprintf("%d", s.m[i]) == id {
			s.m = append(s.m[:i], s.m[i+1:]...)
			break
		}
	}
}

///////////////
// Order
///////////////

type OrderCreate struct {
	Products []OrderProduct `json:"products"`
	Address  string         `json:"address"`
}

type OrderProduct struct {
	Id       int `json:"id"`
	Quantity int `json:"quantity"`
}

type Order struct {
	Id       string         `json:"name"`
	Products []OrderProduct `json:"products"`
	Delivery Delivery       `json:"delivery"`
	Status   string         `json:"status"`
}

type Delivery struct {
	Time string `json:"time"`
}

type OrderStorage interface {
	SetOrder(*OrderCreate)
	GetOrder(id string) (*Order, bool)
	GetOrders(offset int, limit int) ([]Order, error)
}

type ordersStorage struct {
	ch *cache.Cache
	m  []string
}

func (s *ordersStorage) SetOrder(o OrderCreate) *Order {
	order := Order{Id: NextOrderID(), Products: o.Products, Delivery: Delivery{Time: "20022-10-22 12:00:00"}, Status: "new"}

	s.ch.Set(order.Id, &order, cache.DefaultExpiration)
	s.m = append(s.m, order.Id)

	return &order
}

func (s *ordersStorage) GetOrder(id string) (*Order, bool) {
	o, ok := s.ch.Get(id)
	if !ok {
		return nil, false
	}

	return o.(*Order), true
}

func (s *ordersStorage) GetOrders(offset int, limit int) ([]Order, error) {
	if offset > len(s.m) {
		return []Order{}, errors.New("you looking order out of range")
	}

	cap := limit
	if limit == 0 {
		cap = len(s.m) - offset
	}
	result := make([]Order, 0, cap)

	for i := offset; i < len(s.m); i++ {
		if limit != 0 && i >= offset+limit {
			break
		}

		prod, ok := s.GetOrder(s.m[i])
		if !ok {
			fmt.Println(i)
			return []Order{}, errors.New("not found entry by id " + s.m[i])
		}
		result = append(result, *prod)
	}

	return result, nil
}

func NextOrderID() string {
	return uuid.NewString()
}
