package main

import (
	"context"
	"errors"
	"fmt"
	"time"
)

const TimeLimit = 3000
const SomeA = 40

func main() {
	chn := make(chan int)

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Duration(TimeLimit*int(time.Millisecond)))
	defer cancel()

	go func() {
		res, err := LegacyFunction(SomeA)
		if err != nil {
			fmt.Printf("Legacy has error: %s", err.Error())

			close(chn)

			return
		}

		chn <- res
	}()

	select {
	case result, ok := <-chn:
		if ok != false {
			fmt.Printf("B from legacy = %d", result)
		}
		break
	case <-ctx.Done():
		fmt.Println("Context time limit")
	}
}

func LegacyFunction(a int) (int, error) {

	// какая-то крайне полезная работа
	b := a * 10
	if b >= 1000 {
		return 0, errors.New("error b > 1000")
	}

	time.Sleep(time.Duration(b * int(time.Millisecond)))

	return b, nil
}
